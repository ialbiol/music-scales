import { Component, OnInit } from '@angular/core';

import { ScalesService } from './scales.service';
import { Note } from './note';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ScalesService]
})

export class AppComponent implements OnInit {
  title: Note;
  scale: Note[];
  majorScale: Note[];
  guitar = Array();
  majorGuitar = Array();
  pentaGuitar = Array();

  constructor(private ScalesService: ScalesService) { }

  getData(): void{
    this.title = this.ScalesService.translateNote(10);

    this.scale = this.ScalesService.getColourScale(3);

    this.majorScale = this.ScalesService.getMajorScale(3);

    this.guitar = this.ScalesService.getGuitar();

    this.majorGuitar = this.ScalesService.getGuitarMajorScale(1, 0);

    this.pentaGuitar = this.ScalesService.getGuitarPentatonicScale(1, true, 1);
  }

  ngOnInit(): void {
    this.getData();
  }

}
