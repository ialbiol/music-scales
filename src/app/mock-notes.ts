import { Note } from './note';

const NOTE_A : number = 0;
const NOTE_AB: number = 1;
const NOTE_B : number = 2;
const NOTE_C : number = 3;
const NOTE_CB: number = 4;
const NOTE_D : number = 5;
const NOTE_DB: number = 6;
const NOTE_E : number = 7;
const NOTE_F : number = 8;
const NOTE_FB: number = 9;
const NOTE_G : number = 10;
const NOTE_GB: number = 11;

export const TRANSLATION: Note[] = [
    { id: NOTE_A , note: 'A',  name: 'La'  , octave: '1' },
    { id: NOTE_AB, note: 'Ab', name: 'Lab' , octave: '1' },
    { id: NOTE_B , note: 'B',  name: 'Si'  , octave: '1' },
    { id: NOTE_C , note: 'C',  name: 'Do'  , octave: '1' },
    { id: NOTE_CB, note: 'Cb', name: 'Dob' , octave: '1' },
    { id: NOTE_D , note: 'D',  name: 'Re'  , octave: '1' },
    { id: NOTE_DB, note: 'Db', name: 'Reb' , octave: '1' },
    { id: NOTE_E , note: 'E',  name: 'Mi'  , octave: '1' },
    { id: NOTE_F , note: 'F',  name: 'Fa'  , octave: '1' },
    { id: NOTE_FB, note: 'Fb', name: 'Fab' , octave: '1' },
    { id: NOTE_G , note: 'G',  name: 'Sol' , octave: '1' },
    { id: NOTE_GB, note: 'Gb', name: 'Solb', octave: '1' }
];

export class Notes {

    static Note_A() : number { return NOTE_A;  }
    static Note_AB(): number { return NOTE_AB; }
    static Note_B() : number { return NOTE_B;  }
    static Note_C() : number { return NOTE_C;  }
    static Note_CB(): number { return NOTE_CB; }
    static Note_D() : number { return NOTE_D;  }
    static Note_DB(): number { return NOTE_DB; }
    static Note_E() : number { return NOTE_E;  }
    static Note_F() : number { return NOTE_F;  }
    static Note_FB(): number { return NOTE_FB; }
    static Note_G() : number { return NOTE_G;  }
    static Note_GB(): number { return NOTE_GB; }

    static Note_Empty(): Note { return { id: -1, note: '', name: '', octave: '' }; }

    static getColourScaleArray(): Array<number> {
        return [
            NOTE_A, NOTE_AB, NOTE_B, NOTE_C, NOTE_CB, NOTE_D, NOTE_DB, NOTE_E, NOTE_F, NOTE_FB, NOTE_G, NOTE_GB,
            NOTE_A, NOTE_AB, NOTE_B, NOTE_C, NOTE_CB, NOTE_D, NOTE_DB, NOTE_E, NOTE_F, NOTE_FB, NOTE_G, NOTE_GB,
            NOTE_A, NOTE_AB, NOTE_B, NOTE_C, NOTE_CB, NOTE_D, NOTE_DB, NOTE_E, NOTE_F, NOTE_FB, NOTE_G, NOTE_GB
        ];
    }

    static getGuitarIndex(): Array<number> {
        return [
            NOTE_F, NOTE_C, NOTE_GB, NOTE_DB, NOTE_AB, NOTE_F
            //NOTE_E, NOTE_B, NOTE_G, NOTE_D, NOTE_A ,NOTE_E
        ];
    }

    static getMajorScaleIndex(): Array<number> {
        return [0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19, 21, 23, 24];
    }

    static getGuitarMajorScaleIndex(ScaleNumber): Array<Array<number>> {
        if(ScaleNumber == 1){
            return [
                [2, 4, 6],
                [2, 4, 5],
                [1, 3, 4],
                [1, 2, 4],
                [1, 2, 4],
                [0, 2, 4]
            ];
        }
        if(ScaleNumber == 2){
            return [
                [4, 6, 7],
                [4, 5, 7],
                [3, 4, 6],
                [2, 4, 6],
                [2, 4, 6],
                [2, 4, 6]
            ];
        }
        if(ScaleNumber == 3){
            return [
                [6, 7, 9],
                [5, 7, 9],
                [4, 6, 8],
                [4, 6, 8],
                [4, 6, 7],
                [4, 6, 7]
            ];
        }
        if(ScaleNumber == 4){
            return [
                [7, 9, 11],
                [7, 9, 11],
                [6, 8, 9],
                [6, 8, 9],
                [6, 7, 9],
                [6, 7, 9]
            ];
        }
        if(ScaleNumber == 5){
            return [
                [9, 11, 12],
                [9, 11, 12],
                [8, 9, 11],
                [8, 9, 11],
                [7, 9, 11],
                [7, 9, 11]
            ];
        }
        if(ScaleNumber == 6){
            return [
                [11, 12, 14],
                [11, 12, 14],
                [9, 11, 13],
                [9, 11, 13],
                [9, 11, 13],
                [9, 11, 12]
            ];
        }
        if(ScaleNumber == 7){
            return [
                [12, 14, 16],
                [12, 14, 16],
                [11, 13, 15],
                [11, 13, 14],
                [11, 13, 14],
                [11, 12, 14],
            ];
        }
    }

    static getGuitarPentatonicScaleIndex(ScaleNumber, includeBlueNote): Array<Array<number>> {
        if(ScaleNumber == 1){
            if(includeBlueNote == true){
                return [
                    [2, 4],
                    [2, 3, 4],
                    [1, 4],
                    [1, 4],
                    [2, 4, 5],
                    [2, 4]
                ];
            }
            return [
                [2, 4],
                [2, 4],
                [1, 4],
                [1, 4],
                [2, 4],
                [2, 4]
            ];
        }
        if(ScaleNumber == 2){
            if(includeBlueNote == true){
                return [
                    [4, 7],
                    [4, 7],
                    [4, 6, 7],
                    [4, 6],
                    [4, 5, 6],
                    [4, 7]
                ];
            }
            return [
                [4, 7],
                [4, 7],
                [4, 6],
                [4, 6],
                [4, 6],
                [4, 7]
            ];
        }
        if(ScaleNumber == 3){
            if(includeBlueNote == true){
                return [
                    [7, 9, 10],
                    [7, 9],
                    [6, 7, 8],
                    [6, 9],
                    [6, 9],
                    [7, 9, 10]
                ];
            }
            return [
                [7, 9],
                [7, 9],
                [6, 8],
                [6, 9],
                [6, 9],
                [7, 9]
            ];
        }
        if(ScaleNumber == 4){
            if(includeBlueNote == true){
                return [
                    [9, 10, 11],
                    [9, 12],
                    [8, 11],
                    [9, 11, 12],
                    [9, 11],
                    [9, 10, 11]
                ];
            }
            return [
                [9, 11],
                [9, 12],
                [8, 11],
                [9, 11],
                [9, 11],
                [9, 11]
            ];
        }
        if(ScaleNumber == 5){
            if(includeBlueNote == true){
                return [
                    [11, 14],
                    [12, 14, 15],
                    [11, 13],
                    [11, 12, 13],
                    [11, 14],
                    [11, 14]
                ];
            }
            return [
                [11, 14],
                [12, 14],
                [11, 13],
                [11, 13],
                [11, 14],
                [11, 14]
            ];
        }
    }

}