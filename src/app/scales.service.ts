import { Injectable } from '@angular/core';

import { Note } from './note';
import { Notes } from './mock-notes';
import { TRANSLATION } from './mock-notes';

const COLOUR_SCALE_MAX_NOTES: number = 23;
const MAJOR_SCALE_MAX_NOTES : number = 15;

@Injectable()

export class ScalesService {

    translateNote(NumNote): Note {
        return TRANSLATION[NumNote];
    }

    moveFred(fredPosition, fredsToMove): number {
        var realFred = fredPosition + fredsToMove;
        if(realFred <= 0) { realFred = realFred + 12; }
        if(realFred > 24) { realFred = realFred - 24; }

        return realFred;
    }

    getColourScale(mainNote): Array<Note> {
        var scale = [];
        var fullScale = Notes.getColourScaleArray();

        for(var nnote = 0; nnote <= COLOUR_SCALE_MAX_NOTES; nnote++){
            scale[nnote] = this.translateNote( fullScale[mainNote + nnote] );
        }

        return scale;
    }

    getGuitar(): Array<Array<Note>> {
        var guitar = Array();
        var lines = Notes.getGuitarIndex();

        for(var num = 0; num < 6; num++){
            guitar[num] = this.getColourScale( lines[num] );
        }

        return guitar;
    }

    getMajorScale(mainNote): Array<Note> {
        var scale = [];
        var colourScale = this.getColourScale(mainNote);
        var majorScaleIndex = Notes.getMajorScaleIndex();

        for(var nnote = 0; nnote <= MAJOR_SCALE_MAX_NOTES; nnote++){
            scale[nnote] = colourScale[ majorScaleIndex[nnote] ];
        }

        return scale;
    }

    getGuitarMajorScale(number, fredsToMove): Array<Array<Note>> {
        var guitar = this.getGuitar();
        var scale = Notes.getGuitarMajorScaleIndex(number);

        for(var num = 0; num < 6; num++){
            var line = guitar[num];
            for(var fredNum = 0; fredNum < line.length; fredNum++){
                if(scale[num].indexOf(fredNum) == -1){
                    guitar[num][fredNum] = Notes.Note_Empty();
                }
            }
        }

        return guitar;
    }


    getGuitarPentatonicScaleIndex(ScaleNumber, includeBlueNote, fredsToMove){
        var index = Notes.getGuitarPentatonicScaleIndex(ScaleNumber, includeBlueNote);

        if(ScaleNumber == 1){
            if((fredsToMove == -2) || (fredsToMove == -3) || (fredsToMove == -4) || (fredsToMove == -5)){
                fredsToMove = fredsToMove + 12;
            }
        }
        if(ScaleNumber == 2){
            if((fredsToMove == -5) || (fredsToMove == -6) || (fredsToMove == -7) || (fredsToMove == -8)){
                fredsToMove = fredsToMove + 12;
            }
        }
        if(ScaleNumber == 3){
            if((fredsToMove == -7) || (fredsToMove == -8) || (fredsToMove == -9) || (fredsToMove == -10)){
                fredsToMove = fredsToMove + 12;
            }
        }
        if(ScaleNumber == 4){
            if((fredsToMove == -9) || (fredsToMove == -10) || (fredsToMove == -11) || (fredsToMove == -12)){
                fredsToMove = fredsToMove + 12;
            }
            if(fredsToMove == 12){
                fredsToMove = fredsToMove - 12;
            }
        }
        if(ScaleNumber == 5){
            if((fredsToMove == 10) || (fredsToMove == 11) || (fredsToMove == 12) ){
                fredsToMove = fredsToMove - 12;
            }
        }

        if(fredsToMove != 0){
            for(var num = 0; num < 6; num++){
                var line = index[num];
                for(var fredNum = 0; fredNum < line.length; fredNum++){
                    index[num][fredNum] = this.moveFred(index[num][fredNum], fredsToMove);
                }
            }
        }

        return index;
    }

    getGuitarPentatonicScale(number, includeBlueNote, fredsToMove): Array<Array<Note>> {
        var guitar = this.getGuitar();
        var scale = this.getGuitarPentatonicScaleIndex(number, includeBlueNote, fredsToMove);

        for(var num = 0; num < 6; num++){
            var line = guitar[num];
            for(var fredNum = 0; fredNum < line.length; fredNum++){
                if(scale[num].indexOf(fredNum) == -1){
                    guitar[num][fredNum] = Notes.Note_Empty();
                }
            }
        }

        return guitar;
    }

}