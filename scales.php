<?php

class Scales {

    const NOTE_A = 1;
    const NOTE_AB = 2;
    const NOTE_B = 3;
    const NOTE_C = 4;
    const NOTE_CB = 5;
    const NOTE_D = 6;
    const NOTE_DB = 7;
    const NOTE_E = 8;
    const NOTE_F = 9;
    const NOTE_FB = 10;
    const NOTE_G = 11;
    const NOTE_GB = 12;

    const COLOUR_SCALE_MAX_NOTES = 25;
    const MAJOR_SCALE_MAX_NOTES = 15;


    private static function getTranslationNoteArray(){
        return array(
            self::NOTE_A  => 'A',  self::NOTE_AB => 'Ab', self::NOTE_B  => 'B',  self::NOTE_C  => 'C',
            self::NOTE_CB => 'Cb', self::NOTE_D  => 'D',  self::NOTE_DB => 'Db', self::NOTE_E  => 'E',
            self::NOTE_F  => 'F',  self::NOTE_FB => 'Fb', self::NOTE_G  => 'G',  self::NOTE_GB => 'Gb',
        );
    }

    private static function getColourScaleArray(){
        return array(
            1  => self::NOTE_A,  2  => self::NOTE_AB, 3  => self::NOTE_B,  4  => self::NOTE_C,
            5  => self::NOTE_CB, 6  => self::NOTE_D,  7  => self::NOTE_DB, 8  => self::NOTE_E,
            9  => self::NOTE_F,  10 => self::NOTE_FB, 11 => self::NOTE_G,  12 => self::NOTE_GB,
            13 => self::NOTE_A,  14 => self::NOTE_AB, 15 => self::NOTE_B,  16 => self::NOTE_C,
            17 => self::NOTE_CB, 18 => self::NOTE_D,  19 => self::NOTE_DB, 20 => self::NOTE_E,
            21 => self::NOTE_F,  22 => self::NOTE_FB, 23 => self::NOTE_G,  24 => self::NOTE_GB,
            25 => self::NOTE_A,  26 => self::NOTE_AB, 27 => self::NOTE_B,  28 => self::NOTE_C,
            29 => self::NOTE_CB, 30 => self::NOTE_D,  31 => self::NOTE_DB, 32 => self::NOTE_E,
            33 => self::NOTE_F,  34 => self::NOTE_FB, 35 => self::NOTE_G,  36 => self::NOTE_GB,
        );
    }

    private static function getGuitarIndex(){
        return array(
            1  => self::NOTE_F, //self::NOTE_E,
            2  => self::NOTE_C, //self::NOTE_B,
            3  => self::NOTE_GB, //self::NOTE_G,
            4  => self::NOTE_DB, //self::NOTE_D,
            5  => self::NOTE_AB, //self::NOTE_A,
            6  => self::NOTE_F, //self::NOTE_E
        );
    }

    private static function getMajorScaleIndex(){
        return array(
            1  => 1,  2  => 3,  3  => 5,  4  => 6,
            5  => 8,  6  => 10, 7  => 12, 8  => 13,
            9  => 15, 10 => 17, 11 => 18, 12 => 20,
            13 => 22, 14 => 24, 15 => 25
        );
    }

    private static function getGuitarMajorScaleIndex($ScaleNumber = 1){
        switch($ScaleNumber){
            case 1:
                return array(
                    1  => array(1 => 3, 2 => 5, 3 => 7),
                    2  => array(1 => 3, 2 => 5, 3 => 6),
                    3  => array(1 => 2, 2 => 4, 3 => 5),
                    4  => array(1 => 2, 2 => 3, 3 => 5),
                    5  => array(1 => 2, 2 => 3, 3 => 5),
                    6  => array(1 => 1, 2 => 3, 3 => 5)
                );
            break;
            case 2:
                return array(
                    1  => array(1 => 5, 2 => 7, 3 => 8),
                    2  => array(1 => 5, 2 => 6, 3 => 8),
                    3  => array(1 => 4, 2 => 5, 3 => 7),
                    4  => array(1 => 3, 2 => 5, 3 => 7),
                    5  => array(1 => 3, 2 => 5, 3 => 7),
                    6  => array(1 => 3, 2 => 5, 3 => 7)
                );
            break;
            case 3:
                return array(
                    1  => array(1 => 7, 2 => 8, 3 => 10),
                    2  => array(1 => 6, 2 => 8, 3 => 10),
                    3  => array(1 => 5, 2 => 7, 3 => 9),
                    4  => array(1 => 5, 2 => 7, 3 => 9),
                    5  => array(1 => 5, 2 => 7, 3 => 8),
                    6  => array(1 => 5, 2 => 7, 3 => 8)
                );
            break;
            case 4:
                return array(
                    1  => array(1 => 8, 2 => 10, 3 => 12),
                    2  => array(1 => 8, 2 => 10, 3 => 12),
                    3  => array(1 => 7, 2 => 9, 3 => 10),
                    4  => array(1 => 7, 2 => 9, 3 => 10),
                    5  => array(1 => 7, 2 => 8, 3 => 10),
                    6  => array(1 => 7, 2 => 8, 3 => 10)
                );
            break;
            case 5:
                return array(
                    1  => array(1 => 10, 2 => 12, 3 => 13),
                    2  => array(1 => 10, 2 => 12, 3 => 13),
                    3  => array(1 => 9, 2 => 10, 3 => 12),
                    4  => array(1 => 9, 2 => 10, 3 => 12),
                    5  => array(1 => 8, 2 => 10, 3 => 12),
                    6  => array(1 => 8, 2 => 10, 3 => 12)
                );
            break;
            case 6:
                return array(
                    1  => array(1 => 12, 2 => 13, 3 => 15),
                    2  => array(1 => 12, 2 => 13, 3 => 15),
                    3  => array(1 => 10, 2 => 12, 3 => 14),
                    4  => array(1 => 10, 2 => 12, 3 => 14),
                    5  => array(1 => 10, 2 => 12, 3 => 14),
                    6  => array(1 => 10, 2 => 12, 3 => 13)
                );
            break;
            case 7:
                return array(
                    1  => array(1 => 13, 2 => 15, 3 => 17),
                    2  => array(1 => 13, 2 => 15, 3 => 17),
                    3  => array(1 => 12, 2 => 14, 3 => 16),
                    4  => array(1 => 12, 2 => 14, 3 => 15),
                    5  => array(1 => 12, 2 => 14, 3 => 15),
                    6  => array(1 => 12, 2 => 13, 3 => 15)
                );
            break;
        }
    }

    private function getGuitarPentatonicScaleIndex($ScaleNumber = 1, $includeBlueNote = false, $fredsToMove = 0){
        $index = array();

        switch($ScaleNumber){
            case 1:
                $index =  array(
                    1  => array(1 => 3, 2 => 5),
                    2  => array(1 => 3, 2 => 5),
                    3  => array(1 => 2, 2 => 5),
                    4  => array(1 => 2, 2 => 5),
                    5  => array(1 => 3, 2 => 5),
                    6  => array(1 => 3, 2 => 5)
                );
                if($includeBlueNote){
                    $index[2][3] = 4;
                    $index[5][3] = 6;
                }
                if(in_array ($fredsToMove, array(-2, -3, -4, -5))){
                    $fredsToMove = $fredsToMove + 12;
                }
            break;
            case 2:
                $index =  array(
                    1  => array(1 => 5, 2 => 8),
                    2  => array(1 => 5, 2 => 8),
                    3  => array(1 => 5, 2 => 7),
                    4  => array(1 => 5, 2 => 7),
                    5  => array(1 => 5, 2 => 7),
                    6  => array(1 => 5, 2 => 8)
                );
                if($includeBlueNote){
                    $index[3][3] = 8;
                    $index[5][3] = 6;
                }
                if(in_array ($fredsToMove, array(-5, -6, -7, -8))){
                    $fredsToMove = $fredsToMove + 12;
                }
            break;
            case 3:
                $index =  array(
                    1  => array(1 => 8, 2 => 10),
                    2  => array(1 => 8, 2 => 10),
                    3  => array(1 => 7, 2 => 9),
                    4  => array(1 => 7, 2 => 10),
                    5  => array(1 => 7, 2 => 10),
                    6  => array(1 => 8, 2 => 10)
                );
                if($includeBlueNote){
                    $index[1][3] = 11;
                    $index[3][3] = 8;
                    $index[6][3] = 11;
                }
                if(in_array ($fredsToMove, array(-7, -8, -9, -10))){
                    $fredsToMove = $fredsToMove + 12;
                }
            break;
            case 4:
                $index =  array(
                    1  => array(1 => 10, 2 => 12),
                    2  => array(1 => 10, 2 => 13),
                    3  => array(1 => 9,  2 => 12),
                    4  => array(1 => 10, 2 => 12),
                    5  => array(1 => 10, 2 => 12),
                    6  => array(1 => 10, 2 => 12)
                );
                if($includeBlueNote){
                    $index[1][3] = 11;
                    $index[4][3] = 13;
                    $index[6][3] = 11;
                }
                if(in_array ($fredsToMove, array(-9, -10, -11, -12))){
                    $fredsToMove = $fredsToMove + 12;
                }
                if(in_array ($fredsToMove, array(12))){
                    $fredsToMove = $fredsToMove - 12;
                }
            break;
            case 5:
                $index =  array(
                    1  => array(1 => 12, 2 => 15),
                    2  => array(1 => 13, 2 => 15),
                    3  => array(1 => 12, 2 => 14),
                    4  => array(1 => 12, 2 => 14),
                    5  => array(1 => 12, 2 => 15),
                    6  => array(1 => 12, 2 => 15)
                );
                if($includeBlueNote){
                    $index[2][3] = 16;
                    $index[4][3] = 13;
                }
                if(in_array ($fredsToMove, array(10, 11, 12))){
                    $fredsToMove = $fredsToMove - 12;
                }
            break;
        }
        if($fredsToMove != 0){
            foreach($index as $lineNum => &$line){
                foreach($line as $fredNum => &$fred){
                    $fred = $this->moveFred($fred, $fredsToMove);
                }
            }
        }

        return $index;
    }

    private function translateNote($Note = self::NOTE_A){
        return Scales::getTranslationNoteArray()[$Note];
    }

    private function moveFred($fredPosition = 1, $fredsToMove = 0){
        $realFred = $fredPosition + $fredsToMove;
        if($realFred <= 0){ $realFred = $realFred + 12; }
        if($realFred > 24){ $realFred = $realFred - 24; }
        return $realFred;
    }

    public function getColourScale($MainNote = self::NOTE_A){
        $scale = array();
        $fullScale = Scales::getColourScaleArray();

        for($note = 1; $note <= self::COLOUR_SCALE_MAX_NOTES; $note++){
            $scale[$note] = $this->translateNote( $fullScale[$MainNote + $note - 1] );
        }
        return $scale;
    }

    public function getGuitar(){
        $guitar = array();
        $lines = Scales::getGuitarIndex();

        foreach($lines as $lineNum => $line){
            $guitar[$lineNum] = $this->getColourScale($line);
        }

        return $guitar;
    }

    public function getMajorScale($MainNote = self::NOTE_A){
        $scale = array();
        $colourScale = $this->getColourScale($MainNote);
        $majorScaleIndex = Scales::getMajorScaleIndex();

        for($note = 1; $note <= self::MAJOR_SCALE_MAX_NOTES; $note++){
            $scale[$note] = $colourScale[$majorScaleIndex[$note]];
        }
        return $scale;
    }

    public function getGuitarMajorScale($Number = 1, $fredsToMove = 0){
        $guitar = array();
        $guitar = $this->getGuitar();
        $scale = Scales::getGuitarMajorScaleIndex($Number);

        foreach($guitar as $lineNum => $line){
            foreach($line as $fredNum => $fred){
                if(!in_array ($fredNum, $scale[$lineNum])){
                    $guitar[$lineNum][$fredNum] = '<br />';
                }
            }
        }

        return $guitar;
    }

    public function getGuitarPentatonicScale($Number = 1, $includeBlueNote = false, $fredsToMove = 0){
        $guitar = array();
        $guitar = $this->getGuitar();
        $scale = $this->getGuitarPentatonicScaleIndex($Number, $includeBlueNote, $fredsToMove);

        foreach($guitar as $lineNum => $line){
            foreach($line as $fredNum => $fred){
                if(!in_array ($fredNum, $scale[$lineNum])){
                    $guitar[$lineNum][$fredNum] = '<br />';
                }
            }
        }

        return $guitar;
    }


}
?>