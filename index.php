<?php

include("scales.php");

$scales = new Scales();

//print_r($scales->getColourScale(Scales::NOTE_C), false);

//print_r($scales->getMajorScale(Scales::NOTE_C), false);

//DrawGuitar($scales->getGuitar());

//DrawGuitar($scales->getGuitarMajorScale(1));

for($position = 1; $position <= 5; $position++){
    DrawGuitar($scales->getGuitarPentatonicScale($position, true, -9));
}


function DrawGuitar($guitar){
echo "<div class='mainGuitar' style='background-color: #000;'>";
echo "<table>";
echo "<tr>";
for($cols=1; $cols<=24; $cols++){
    echo "<td style='width: 30px; background-color: #fff'>";
    switch($cols){
        case 3: echo "3";
            break;
        case 5: echo "5";
            break;
        case 7: echo "7";
            break;
        case 9: echo "9";
            break;
        case 12: echo "12";
            break;
        case 15: echo "15";
            break;
        case 17: echo "17";
            break;
        case 19: echo "19";
            break;
        case 21: echo "21";
            break;
        case 24: echo "24";
            break;
        default: echo "<br />" ;
    }
    echo "</td>";
}
echo "</tr>";
for($rows=1; $rows<=6; $rows++){
    echo "<tr>";
    for($cols=1; $cols<=24; $cols++){
        if($guitar[$rows][$cols] == 'C'){
            echo "<td style='width: 30px; border: 1px #000 solid; background-color: #f00'>";
        } else {
            echo "<td style='width: 30px; border: 1px #000 solid; background-color: #fff'>";
        }
        echo $guitar[$rows][$cols];
        echo "</td>";
    }
    echo "</tr>";
}
echo "</div>";
}
?>